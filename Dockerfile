FROM maven:3.8.6-jdk-11-slim AS build

RUN mkdir -p /application
RUN useradd -U spring

WORKDIR application
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN mvn -B package -DskipTests
RUN cp /application/target/*.jar demo-0.0.1-SNAPSHOT.jar
RUN chown -R spring:spring /application

USER spring

FROM openjdk:11-jdk-slim

WORKDIR application

COPY --from=build application/demo-0.0.1-SNAPSHOT.jar ./

EXPOSE 8080
ENTRYPOINT ["java","-Djava.awt.headless=true","-jar","demo-0.0.1-SNAPSHOT.jar"]
